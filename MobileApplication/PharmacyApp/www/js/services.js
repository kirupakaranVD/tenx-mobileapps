angular.module('services', [])

    .service('UserService', function () {

        //for the purpose of this example I will store user data on ionic local storage but you should save it on a database

        var pharmacyusers = {};

        var setUser = function (user_data) {

            if(user_data) {
           
            pharmacyusers.type = user_data[0].loginType.roleTypeId;
            pharmacyusers.name = user_data[0].userFullName;
            pharmacyusers.orgId = user_data[0].organizationId;
            pharmacyusers.partyId = user_data[0].loginType.partyId;
            pharmacyusers.organizationName = user_data[0].currencyUomId.organizationName;
            pharmacyusers.organizationType = user_data[0].currencyUomId.organizationType;
            pharmacyusers.userId = user_data[0].userId;
			pharmacyusers.mailId = user_data[0].emailAddress;
            pharmacyusers.mobileNo = user_data[0].phoneNumber;
            pharmacyusers.gender = user_data[0].gender;
                 
            }else {
                pharmacyusers = {};
            }
        };

        var getUser = function () {
             
            return pharmacyusers;
        };

        return {
            getUser: getUser,
            setUser: setUser
        };
    })
    .service('LoginService', function ($q, $http, UrlEndpoint) {
        this.loginAuth = function (info) {
            var deferred = $q.defer();
            console.log('enpoint url....', UrlEndpoint);
            var baseUrl = UrlEndpoint + 'SignInApp/UserLoginWebService?secureLoginId=' + info.username + '&securePassword=' + info.password;

            $http.get(baseUrl).then(function (response) {

                deferred.resolve(response.data);
            }, function (arguments) {
                deferred.reject(arguments);
            });
            return deferred.promise;
        }

    })
    .service('StocksService', function ($q, $http, $ionicLoading, UserService, UrlEndpoint) {
        this.getBranchList = function () {
            var org = UserService.getUser()
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
            $http.get(UrlEndpoint + 'rest/s1/ViewStock/activatedBranchList?organizationId=' + org.orgId).then(function (response) {

                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };

        this.getMainBranchStocksList = function () {

            var org = UserService.getUser();
            var deferred = $q.defer();

            $http.get(UrlEndpoint + 'rest/s1/ViewStock/stockDetailsBase?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };

        this.getBranchStocksList = function (branchInfoObj) {

            var deferred = $q.defer();
            $http.get(UrlEndpoint + 'rest/s1/ViewStock/stockDetailsBase?loginPartyId=' + branchInfoObj.partyId + '&loginOrgId=' + branchInfoObj.organisationId + '&loginUserId=' + branchInfoObj.userId).then(function (response) {

                deferred.resolve(response);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };



    })
    .service('DashboardService', function ($q, $http, UserService, UrlEndpoint) {

        var org = UserService.getUser();
        this.getSalesData = function () {
            var org = UserService.getUser()
            var deferred = $q.defer();
            http: //172.17.1.232:8080/pharmacy/rest/s1/SalesReturn/dashBordDetailsRest?loginUserId=100052&loginOrgId=100004
                $http.get(UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId).then(function (response) {

                    deferred.resolve(response.data);

                }, function () {
                    deferred.reject(arguments);
                });
            return deferred.promise;
        };

    })
    .service('SalesReportService', function ($q, $http, $ionicLoading, UserService, UrlEndpoint) {
        
        var org = UserService.getUser();

        this.getDailySalesData = function (branchOrgId) {

         console.log('branchOrgId', branchOrgId);
            var deferred = $q.defer();
            var jsonUrl;
            if(branchOrgId) {
                jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/salesHistoryDayWeekDetails?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId+'&branchName='+branchOrgId
            }else {
                jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/salesHistoryDayWeekDetails?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId
            }
            $http.get(jsonUrl).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;
        };

        this.getWeeklySalesData = function (branchOrgId) {

            var deferred = $q.defer();
            
             var jsonUrl;
            if(branchOrgId) {
                jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/salesHistoryDayWeekDetails?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId + '&timePeriod=Week'+'&branchName='+branchOrgId
            }else {
                jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/salesHistoryDayWeekDetails?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId + '&timePeriod=Week'
            }
            
            $http.get(jsonUrl).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;
        };


        this.getMonthlySalesData = function (branchOrgId) {

            var deferred = $q.defer();
             var jsonUrl;
            if(branchOrgId) {
                jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/salesHistoryDetails?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId + '&timePeriod=Month'+'&branchName='+branchOrgId
            }else {
                jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/salesHistoryDetails?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId + '&timePeriod=Month'
            }
            console.log('jsonUrl ', jsonUrl); 
                       
 
            
            $http.get(jsonUrl).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;
        };

            
    this.getYearlySalesData = function (branchOrgId) {
                var deferred = $q.defer(); 
                 var jsonUrl;
            if(branchOrgId) {
                jsonUrl=UrlEndpoint+'rest/s1/SalesReturn/salesHistoryDetails?loginPartyId=' + org.partyId +'&loginOrgId='+ org.orgId+'&loginUserId='+ org.userId+'&timePeriod=Year'+'&branchName='+branchOrgId
            }else {
                jsonUrl=UrlEndpoint+'rest/s1/SalesReturn/salesHistoryDetails?loginPartyId=' + org.partyId +'&loginOrgId='+ org.orgId+'&loginUserId='+ org.userId+'&timePeriod=Year'
            }
         console.log('jsonUrl ', jsonUrl); 
        
        $http.get(jsonUrl).then(function (response) {
                    
                    deferred.resolve(response.data);
        
                }, function () {
                    deferred.reject(arguments);
                });
                return deferred.promise;
            };
    this.getDashBordDetailsDailyData = function(branchOrgId) {
        
        var defered =$q.defer();
        var jsonUrl;
        if(branchOrgId) {
            jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId+'&branchName='+branchOrgId
        }else {
            jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId
        }
        $http.get(jsonUrl).then(function(response) 
            {
                defered.resolve(response.data);
            }, function(){
                defered.reject(arguments);
             
            
        });
        return defered.promise;
    };
    
     this.getDashBordDetailsWeekData = function(branchOrgId) {
        
        var defered =$q.defer();
           var jsonUrl;
        if(branchOrgId) {
            jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId+'&timePeriod=Week'+'&branchName='+branchOrgId
        }else {
             jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId+'&timePeriod=Week'
        }
         
         
        $http.get(jsonUrl).then(function(response) 
            {
                defered.resolve(response.data);
            }, function(){
                defered.reject(arguments);
             
            
        });
        return defered.promise;
    };
      this.getDashBordDetailsMonthData = function(branchOrgId) {
        
        var defered =$q.defer();
          var jsonUrl;
        if(branchOrgId) {
            jsonUrl =UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId+ '&timePeriod=Month'+'&branchName='+branchOrgId 
        }else {
            jsonUrl = UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId+ '&timePeriod=Month'
        }
          
        $http.get(jsonUrl).then(function(response) 
            {
                defered.resolve(response.data);
            }, function(){
                defered.reject(arguments);
             
            
        });
        return defered.promise;
    };
    
     this.getDashBordDetailsYearData = function(branchOrgId) {
        
        var defered =$q.defer();
         var jsonUrl;
        if(branchOrgId) {
            jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId+'&timePeriod=Year'+'&branchName='+branchOrgId 
        }else {
            jsonUrl=UrlEndpoint + 'rest/s1/SalesReturn/dashBordDetailsRest?loginUserId='+org.userId +'&loginOrgId='+org.orgId+'&timePeriod=Year'
        }
         
        $http.get(jsonUrl).then(function(response) 
            {
                defered.resolve(response.data);
            }, function(){
                defered.reject(arguments);
             
            
        });
        return defered.promise;
    };
    })

  

  .service('BestSellerReportService', function ($q, $http, $ionicLoading, UserService, UrlEndpoint) {

        var org = UserService.getUser();

        this.getBestProductData = function (selectedType,branchOrgId) {
           console.log('selectedType123', selectedType);
            var bestSalesurl='';
            if(branchOrgId){
             if(selectedType == 'Daily') {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId+'&branchName='+branchOrgId
                
            }else if(selectedType == 'Weekly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId+'&timePeriod=Week'+'&branchName='+branchOrgId
                
            }else if(selectedType == 'Monthly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId+'&timePeriod=Month'+'&branchName='+branchOrgId
                
            }else {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId+'&timePeriod=Year'+'&branchName='+branchOrgId
            }
            }else {
            
            if(selectedType == 'Daily') {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId
                
            }else if(selectedType == 'Weekly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId+'&timePeriod=Week'
                
            }else if(selectedType == 'Monthly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId+'&timePeriod=Month'
                
            }else {
                bestSalesurl=UrlEndpoint + 'rest/s1/SalesReturn/bestSalesDetailsRest?loginUserId=' + org.userId + '&loginOrgId=' + org.orgId+'&timePeriod=Year'
            }
            
            }
            var deferred = $q.defer();
            if(bestSalesurl) {
            $http.get(bestSalesurl).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            }
            return deferred.promise;
        };
      })

  .service('LogoutService', function ($q, $http, UrlEndpoint) {
        this.logout = function () {
            var deferred = $q.defer();
            console.log('enpoint url....', UrlEndpoint);
            var baseUrl = UrlEndpoint + 'SignInApp/logout';

            $http.get(baseUrl).then(function (response) {

                deferred.resolve(response.data);
            }, function (arguments) {
                deferred.reject(arguments);
            });
            return deferred.promise;
        }

    })