var app = angular.module('starter.stockspagecontroller', [])

//SignInCtrl
app.controller('StocksPageController',function($scope,$ionicModal,$ionicLoading,UserService,StocksService,$location) {
    
    
    // ionic page enter 
    $scope.pharmacies = [];
    $scope.stocksDetails = {
        
        stocksListArray : []
    };
    $scope.listofpharmacies = {
        branchDetailsList : []
    };
     $ionicLoading.show({
                                content: 'Loading',
                                animation: 'fade-in',
                                showBackdrop: true,
                                maxWidth: 200,
                                showDelay: 0,
                                duration: 5000
                            });
    $scope.$on('$ionicView.enter', function(){
        
        var pharmacyName = UserService.getUser(); 
         if(pharmacyName.userId) {
        $scope.branchObj.branchName = pharmacyName.name;
        $scope.branchObj.branchType = pharmacyName.organizationName;
        $scope.branchObj.Type = pharmacyName.organizationName;
        
         $scope.branchObj.orgType=pharmacyName.organizationType;
        StocksService.getBranchList().then(function(response){
            console.log('Branches details.....',response.branchDetailsList);
            $scope.listofpharmacies.branchDetailsList = response.branchDetailsList;
            $ionicLoading.hide();
        },function(error){
            console.log('error');
            $ionicLoading.hide();
        });
        StocksService.getMainBranchStocksList().then(function(response){
           
            $scope.stocksDetails.stocksListArray = response.salesHistoryList;
            //console.log($scope.stocksDetails.stocksListArray);
            
        },function(error){
                
            console.log("can't fetch stock list..try again...");
        });
           }else {
               $location.path('/userlogin');
               }
        
    });
    
    
    
    $scope.search = {};
    $scope.query = {};
    $scope.branchObj = {
        
        branchName : "Select Pharmacy",
        branchStocks:[]
    };
    
      $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
    
    $scope.setBranch = function(pharmacyObj){
         var currentOrg=$scope.branchObj.Type;
       
         
        if(pharmacyObj == currentOrg) {
            
           $scope.branchObj.branchType = pharmacyObj; 
        
           StocksService.getBranchList().then(function(response){
            console.log('Branches details.....',response.branchDetailsList);
            $scope.listofpharmacies.branchDetailsList = response.branchDetailsList;
            $ionicLoading.hide();
            $scope.modal.hide();
        },function(error){
            console.log('error');
             $ionicLoading.hide();
            $scope.modal.hide();
        });
            StocksService.getMainBranchStocksList().then(function(response){
            $scope.stocksDetails.stocksListArray = response.salesHistoryList;
             
        },function(error){
                
            console.log("can't fetch stock list..try again...");
        });  
            
        }else {  
        
        $scope.branchObj.branchType = pharmacyObj.organizationName;
        
         var branchDetail = {

            organisationId: pharmacyObj.orginatationId,
            partyId: pharmacyObj.partyId,
            userId: pharmacyObj.userId
        };
             
         
        
        
        StocksService.getBranchStocksList(branchDetail).then(
        function(response){
           
             $scope.stocksDetails.stocksListArray = response.data.salesHistoryList;
            console.log('branch stock...',$scope.branchObj.branchStocks);
            $scope.search = {};
            $ionicLoading.hide();
            $scope.modal.hide();
        },function(error){
            $ionicLoading.hide();
            console.log('Error');
            console.log('error cant fetch branch stock');
        });
          
        }
           $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
    };

    
    
    
    
})