var app = angular.module('starter.bestpagecontroller', [])

//SignInCtrl
app.controller('BestPageController', function ($scope,$ionicModal,$ionicPopup,$ionicLoading,UserService,StocksService,BestSellerReportService,$timeout,$location) {

      $scope.data =  [];
    $scope.pieChartData=[];
      $scope.checkedReportType;
      $scope.branchObj = {
           branchName : "Select Pharmacy",
        branchStocks:[]
      };
    $scope.listofpharmacies = {
        branchDetailsList: []
    };
     var selectedType='';
     $scope.choice = {
        val: 'Daily'
    };
   $scope.showSortPopup = showSortPopup;  
  
     $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0,
            duration: 5000                          
                            }); 
    $scope.$on('$ionicView.enter', function () {

        var pharmacyName = UserService.getUser();
        if(pharmacyName.userId) {
        $scope.branchObj.branchName = pharmacyName.name;
        $scope.branchObj.branchType = pharmacyName.organizationName;
       $scope.branchObj.Type = pharmacyName.organizationName;
        
       $scope.branchObj.orgType=pharmacyName.organizationType;
        
        if (pharmacyName.type == 'ChainPharmacyAdmin') {
            StocksService.getBranchList().then(function (response) {
                
                $scope.listofpharmacies.branchDetailsList = response.branchDetailsList;
                 $ionicLoading.hide();
            }, function (error) {
                console.log('error');
                 $ionicLoading.hide();
            });
        }

       $scope.checkedReportType='Daily';  
          getBestSaleReport('Daily','');
        pieChart();
      }else {
               $location.path('/userlogin');
               }  
        
});
    
    $scope.search = {};
    $scope.query = {};
    
    $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
    
  $scope.setBranch = function(pharmacyObj){
      
       $ionicLoading.show({ content: 'Loading',
                                animation: 'fade-in',
                                showBackdrop: true,
                                maxWidth: 200,
                                showDelay: 0,
                                duration: 200
                            });
        $timeout(function () { 
      var currentOrg=$scope.branchObj.Type;  
       
       if(pharmacyObj == currentOrg) { 
      $scope.branchObj.branchType = pharmacyObj;  
            $scope.orginatationId='';    
         }else {    
             $scope.branchObj.branchType = pharmacyObj.organizationName;
            $scope.orginatationId = pharmacyObj.orginatationId;
              }
       $scope.search = {};
        $scope.modal.hide();
      getBestSaleReport($scope.checkedReportType,$scope.orginatationId); }, 50);
       
    };
    
   function showSortPopup() {
         var branchOrgId=$scope.orginatationId
       
       console.log('checked value' ,selectedType);
       if(selectedType) {
            $scope.choice.val=selectedType;
       }
      
        $ionicPopup.show({
            title: 'choose Report type',
            templateUrl: 'templates/report-option-template.html',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel',
                    type: 'button-stable',
                    onTap: function (e) {
                        $scope.reportType.val = 'Daily';
                        
                    }
                        },
                {
                    text: 'Ok',
                    type: 'button-positive',
                    onTap: function (e) {
                         selectedType=$scope.choice.val;
                        $scope.checkedReportType=$scope.choice.val;
                        
                            getBestSaleReport(selectedType,branchOrgId);
                            
                              $scope.reportType = $scope.choice;
                            $scope.choice = {
                            val: 'Daily'
                        };
                    }
                        },
                    ]

        });
 

    };  
   
    
 function getBestSaleReport(selectedType,branchOrgId){
      BestSellerReportService.getBestProductData(selectedType,branchOrgId).then(function (response) {
                                 var dailyReporthistory = [];
                            $scope.pieChartData=response.bestSaleUserList;    
                            if($scope.pieChartData) {
                                 $scope.data = response.bestSaleUserList
                                pieChart();
                            }
                                }, function (error) {
                                console.log('report services error....', response);
                            });
 }
    
    function pieChart() {
            $scope.options = {
        chart: {
            type: 'pieChart',
            height: 300,
            x: function (d) {
                return d.orgName;
            },
            y: function (d) {
                return d.Percentage;
            },
            showLabels: true,
            labelsOutside:true,
            labelType: 'percent',
            showLegend: false,
            duration: 500,
            labelThreshold: 0.01,
            labelSunbeamLayout: true,
            
        }
    };
    }
    
})

 
