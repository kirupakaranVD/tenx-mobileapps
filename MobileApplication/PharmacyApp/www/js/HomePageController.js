var app = angular.module('starter.homepagecontroller', [])

//SignInCtrl
app.controller('HomePageController',function($scope,$ionicLoading,DashboardService,UserService,$location) {
    
    $scope.defaultDay = "";
     $scope.salesRecord = "";
      $scope.currentDate = "";
    $scope.$on('$ionicView.afterEnter', function(){
        var pharmacyName = UserService.getUser(); 
         if(pharmacyName.userId) { 
        DashboardService.getSalesData().then(function(response){
            $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 500
          });
           $scope.salesRecord = response.salesHistoryList[0];
            console.log("sales dashboard details.....",response);
            var d = new Date($scope.salesRecord);
            var n = d.getDay();
            var date = new Date();
             $scope.currentDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
            getDayFromDate(n);
            $ionicLoading.hide();
        },function(error){
            
                
                var d = new Date();
                var n = d.getDay();
                getDayFromDate(n);
                $ionicLoading.hide();
            
            
        })
        }else {
               $location.path('/userlogin');
               }
    });
    
    
    var getDayFromDate = function(dayNum){
        
        switch(dayNum){
                    
                case 0 :
                   
                    ($scope.salesRecord != undefined) ? $scope.salesRecord.day = "Sunday" : $scope.defaultDay = "Sunday";
                
                break;
                    
                case 1 :
                     ($scope.salesRecord != undefined) ? $scope.salesRecord.day = "Monday" : $scope.defaultDay = "Monday";
                break;
                     case 2 :
                    ($scope.salesRecord != undefined) ? $scope.salesRecord.day = "Tuesday" : $scope.defaultDay = "Tuesday";
                    
                break;
                     case 3 :
                     ($scope.salesRecord != undefined) ? $scope.salesRecord.day = "Wednesday" : $scope.defaultDay = "Wednesday";
                
                break;
                  case 4 :
                    ($scope.salesRecord != undefined) ? $scope.salesRecord.day = "Thursday" : $scope.defaultDay = "Thursday";
                
                break;
                    
                 case 5 :
                    ($scope.salesRecord != undefined) ? $scope.salesRecord.day = "Friday" : $scope.defaultDay = "Friday";
                
                break;
                     case 6 :
                   ($scope.salesRecord != undefined) ? $scope.salesRecord.day = "Saturday" : $scope.defaultDay = "Saturday";
                break;
                     default :
                   
                break;
                    
            }
            
        
    };
    
   
    
})