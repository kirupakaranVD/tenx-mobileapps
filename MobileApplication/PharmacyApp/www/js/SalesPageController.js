var app = angular.module('starter.salespagecontroller', []);

app.controller('SalesPageController', function ($scope, $ionicModal, $ionicPopup, $ionicLoading, UserService, StocksService, SalesReportService,$timeout,$location) {

    
    $scope.branchObj = {};
    $scope.yearDashBoradData = {};
    $scope.chartVals=[];
     $scope.dayDashBoradData = {};
    
    $scope.monthDashBoradData = {};
    
    $scope.weekDashBoradData = {};
    
    
    $scope.listofpharmacies = {
        branchDetailsList: []
    };
    $scope.choice = {
        val: 'Daily'
    };
    $scope.reportType = {
        val: 'Daily'
    };
    $scope.checkedReportType;
    $scope.showSortPopup = showSortPopup;
    $scope.dataDR = [
        {
            values: []

                }
            ];

    $scope.dataWR = [
        {
            values: [

                ]
            }
        ];
    $scope.dataMR = [
        {
            values: [

                ]
            }
        ];
 $scope.dataYR = [
        {
            values: [
                ]
            }
        ];
   $ionicLoading.show({
                                content: 'Loading',
                                animation: 'fade-in',
                                showBackdrop: true,
                                maxWidth: 200,
                                showDelay: 0,
                                duration: 5000
                            });
    $scope.$on('$ionicView.enter', function () {
      
        
        var pharmacyName = UserService.getUser();
        if(pharmacyName.userId)
        {
        $scope.branchObj.branchName = pharmacyName.name;
        $scope.branchObj.branchType = pharmacyName.organizationName;
        $scope.branchObj.Type = pharmacyName.organizationName;
         var branchOrgId=$scope.orginatationId
        $scope.branchObj.orgType=pharmacyName.organizationType;
        if (pharmacyName.type == 'ChainPharmacyAdmin') {
            StocksService.getBranchList().then(function (response) {
                 $scope.listofpharmacies.branchDetailsList = response.branchDetailsList;
                 $ionicLoading.hide();
            }, function (error) {
                console.log('error');
            });
        }
        $scope.checkedReportType='Daily';
        getReportData(branchOrgId,$scope.checkedReportType)
        
    }else {
               $location.path('/userlogin');
               }
        
        

    });

    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.setBranch = function (pharmacyObj) {
         $ionicLoading.show({ content: 'Loading',
                                animation: 'fade-in',
                                showBackdrop: true,
                                maxWidth: 200,
                                showDelay: 0,
                                duration: 5000
                            });
        $timeout(function () {
        
        var currentOrg=$scope.branchObj.Type;
           
        if(pharmacyObj == currentOrg) {
           $scope.branchObj.branchType = pharmacyObj;  
            $scope.orginatationId='';
            
        }else {
           $scope.branchObj.branchType = pharmacyObj.organizationName;
            $scope.orginatationId = pharmacyObj.orginatationId;
            }
        $scope.search = {};
             $ionicLoading.hide();
         $scope.modal.hide();
                 getReportData($scope.orginatationId,$scope.checkedReportType) 
 
         }, 500);
        
    };


    function showSortPopup() {
      
        if($scope.checkedReportType) {
            $scope.choice.val=$scope.checkedReportType;
       }
        var branchOrgId=$scope.orginatationId
        $ionicPopup.show({
            title: 'Choose Report Type',
            templateUrl: 'templates/report-option-template.html',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel',
                    type: 'button-stable',
                    onTap: function (e) {
                        $scope.reportType.val = 'Daily';
                       
                    }
                        },
                {
                    text: 'Ok',
                    type: 'button-positive',
                    onTap: function (e) {
                        $scope.checkedReportType=$scope.choice.val;
                        getReportData(branchOrgId,$scope.checkedReportType);
                        
                          $scope.choice = { 
                            val: 'Daily'
                        };
                    }
                        },
                    ]

        });
 

    };
    
    function getReportData(branchOrgId,reportType) {
       console.log('type', reportType);   

        if (reportType == 'Daily') {
            
                SalesReportService.getDailySalesData(branchOrgId).then(function (response) {
                                var dailyReporthistory = [];
                     $scope.chartVals=response.salesHistoryList;
                            if($scope.chartVals) {
                                 $scope.dataDR[0].values = createReortData(response.salesHistoryList, 'Daily');
                                
    $scope.options = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            margin: {
                top: 20,
                right: 20,
                bottom: 50,
                left: 60
            },
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.value;
            },
            showValues: false,
            valueFormat: function (d) {
                return d3.format(',.02f')(d);
            },
            duration: 500,
            xAxis: {

            },
            yAxis: {

            }
        }
    };
                                
                                }
                                
                            }, function (error) {
                                console.log('report services error....', response);
                            });
                           SalesReportService.getDashBordDetailsDailyData(branchOrgId).then(function(response) {
                               $scope.dayDashBoradData=response.salesHistoryList;
                            }, function(error) {
                               console.log('dashboardFor year services error....', response);    
                           });   
                            
                            $scope.reportType = $scope.choice;
                         }
                    
                else if (reportType == 'Weekly') {
                    SalesReportService.getWeeklySalesData(branchOrgId).then(function (response) {
                                var weeklyReporthistory = [];
                              $scope.chartVals=response.salesHistoryList;
                        console.log(" $scope.chartVals", $scope.chartVals);
                            if($scope.chartVals) {
                                $scope.dataWR[0].values = createReortData(response.salesHistoryList, 'Weekly');
                                    $scope.optionsWR = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            margin: {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            
            x: function (d) {
                return d.label;
                
             },
            y: function (d) {
                return d.value;
            },
            showValues: false,
            stacked: true,
            duration: 500,
            xAxis: {
                showMaxMin: false
            },
            yAxis: {
                    
            }
        }
    };
                               }
                               
                            }, function (error) {
                                console.log('report services error....', response);
                            });
                          
                        SalesReportService.getDashBordDetailsWeekData(branchOrgId).then(function(response) {
                               $scope.weekDashBoradData=response.salesHistoryList;
                            }, function(error) {
                               console.log('dashboardFor year services error....', response);    
                           });
                            
                            $scope.reportType = $scope.choice;
                        } 
        
                else if (reportType == 'Monthly') {
                            SalesReportService.getMonthlySalesData(branchOrgId).then(function (response) {
                                var monthlyReporthistory = [];
                                
                                   $scope.chartVals=response.salesHistoryList;
                                if($scope.chartVals) {
                                
                                $scope.dataMR[0].values = createReortData(response.salesHistoryList, 'Monthly');
                                    
                                 $scope.optionsMR = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            margin: {
                top: 20,
                right: 20,
                bottom: 50,
                left: 60
            },
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.value;
            },
            showValues: false,
            valueFormat: function (d) {
                return d3.format('')(d);
            },
            duration: 500,
            xAxis: {

            },
            yAxis: {

            }
        }
    };     

                            }
                            }, function (error) {
                                console.log('report services error....', response);
                            });
                          
                        SalesReportService.getDashBordDetailsMonthData(branchOrgId).then(function(response) {
                               $scope.monthDashBoradData=response.salesHistoryList;
                               console.log($scope.yearDashBoradData);
                           }, function(error) {
                               console.log('dashboardFor year services error....', response);    
                           });
                            
                            $scope.reportType = $scope.choice;
                        } 
        
                else if (reportType== 'Yearly') {
                            SalesReportService.getYearlySalesData(branchOrgId).then(function (response) {
                                var yearlyReporthistory = [];
                                  $scope.chartVals=response.salesHistoryList;
                                if($scope.chartVals) {
                                    $scope.dataYR[0].values = createReortData(response.salesHistoryList, 'Yearly');
                                    $scope.optionsYR = {
          chart: {
                type: 'discreteBarChart',
                height: 200,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 50,
                    left: 80
                },
                x: function(d){return d.label;},
                y: function(d){return d.value;},
                showValues: false,
               
                duration: 500,
                xAxis: {
                 },
                yAxis: {
                  }
            }
    };
                            }
                            }, function (error) {
                                console.log('report services error....', response);
                            });
                            
                           SalesReportService.getDashBordDetailsYearData(branchOrgId).then(function(response) {
                               $scope.yearDashBoradData=response.salesHistoryList;
                               console.log($scope.yearDashBoradData);
                           }, function(error) {
                               console.log('dashboardFor year services error....', response);    
                           });
                            
                            $scope.reportType = $scope.choice;
                        }
 
    }
    
    

   


})



var createReortData = function (res, reportType) {

    var reportDataArray = [];

    for (var i = 0; i < res.length; i++) {

        var reportDataObj = {};
        angular.forEach(res[i], function (value, key) {

            if (reportType == 'Daily') {
                switch (key) {
                    case "Hour":

                        reportDataObj.label = value;
                        break;
                    case "saleValue":
                        reportDataObj.value = value;
                         break;
                    default:

                        break;
                }

            } else if (reportType == 'Weekly') {
                switch (key) {
                    case "Day":

                        reportDataObj.label = value;
                         break;
                    case "saleValue":
                        reportDataObj.value = value;
                          break;
                    default:

                        break;
                }
                  
            } else if (reportType == 'Monthly') {
                switch (key) {
                    case "Day":

                        reportDataObj.label = value;
                        break;
                    case "saleValue":
                        reportDataObj.value = value;
                        break;
                    default:

                        break;
                }

            }else if (reportType == 'Yearly') {
                switch (key) {
                    case "Month":

                        reportDataObj.label = value;
                        break;
                    case "saleValue":
                        reportDataObj.value = value;
                        break;
                    default:

                        break;
                }

            }

        })

        reportDataArray.push(reportDataObj);

    }
    
 
    return reportDataArray;

};
