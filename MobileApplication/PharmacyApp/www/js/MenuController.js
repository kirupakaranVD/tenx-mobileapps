var app = angular.module('starter.menucontroller', []);

app.controller('MenuController', function ($scope, $state, $ionicSideMenuDelegate, $ionicLoading, LogoutService,$translate,UserService,$localstorage,$rootScope) {
 
	$scope.checkVal;
 $scope.Language='Language'; 
        $scope.logout = function () {

        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0,
            duration: 5000
        });

        setTimeout(function () {
            $scope.$apply(function () {
                $ionicLoading.hide();
                LogoutService.logout().then(function (response) {
                    console.log('success logging out');
                    $ionicSideMenuDelegate.toggleLeft();
                    $state.go('userlogin');
                }, function (error) {
                    console.log('logout error....');
                });

            });
        }, 1000);
        UserService.setUser('');     
		console.log('logout ',$rootScope.rememberMe.checked);
		if($rootScope.rememberMe.checked) {
            
        }else {
             $localstorage.setObject('username','');
            $localstorage.setObject('password','');
        }

    };
	$scope.changeLanguage = function (checkVal) {
		console.log('logout error....',checkVal);
		if(checkVal){
			$translate.use('en'); 
			$scope.Language='English';
		}else {
			$translate.use('es'); 
			$scope.Language='Spanish';
		}
	};  
    
})
