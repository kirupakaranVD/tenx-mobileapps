angular.module('starter').directive('quantitySetter',function(){
    return {
    restrict:'AEC',
    templateUrl:"/templates/quantity_setter.html",
     controller:function($scope){
        $scope.increaseQty=function(increment){
          $scope.$broadcast('increase',increment);  
        };
         
        $scope.decreaseQty=function(decrement){
          $scope.$broadcast('decrease',decrement);  
        };
    },
    link:function(scope,elements,attr){
        scope.$on('increase',function(e,increment){
            scope.quantity+=1;
        });
        scope.$on('decrease',function(e,decrement){
            if(scope.quantity>1)
                {
                 scope.quantity-=1;
                }
        });
    }
        }
    
    });