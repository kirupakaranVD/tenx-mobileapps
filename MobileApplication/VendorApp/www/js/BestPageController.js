var app = angular.module('starter.bestpagecontroller', [])

//SignInCtrl
app.controller('BestPageController', function ($scope,$ionicModal,$ionicLoading,UserService,StocksService,$ionicPopup,$location) {

    $scope.search = {};
    $scope.query = {};
    $scope.data =  [];
    
    $scope.choice = {
        val: 'Daily'
    };
    $scope.reportType = {
        val: 'Daily'
    };
    var choiceType='Daily';
    $scope.salesPersionPartyId;
    $scope.showTimePeriodPopup = showTimePeriodPopup;
    
    $scope.branchObj = {
           branchName : "Select Salles Preson",
        branchStocks:[]
      };
    $scope.salesListInVendorPr = {
        salesDetailsList: []
    };
    $scope.options = {
        chart: {
            type: 'pieChart',
            height: 300,
            x: function (d) {
                return d.productId;
            },
            y: function (d) {
                return d.requiredQuantity;
            },
            showLabels: true,
            labelType: 'percent',
            pieLabelsOutside: true,
            showLegend: false,
            duration: 500,
            labelThreshold: 0.01,
            labelSunbeamLayout: true
        }
    };
    
     $scope.$on('$ionicView.beforeEnter', function () {
 var  userName=UserService.getUser();
         if(userName.userId) {
        var pharmacyName = UserService.getUser();
        $scope.branchObj.branchName = pharmacyName.name;
       $scope.branchObj.Type = pharmacyName.name;
        StocksService.getBranchList().then(function(response){
            $scope.salesListInVendorPr.salesDetailsList = response.salesPersonlist1;
            $ionicLoading.hide();
        },function(error){
            console.log('error');
            $ionicLoading.hide();
        });
         StocksService.getMainBranchStocksList().then(function (response) {
             $scope.data = response.bestSaleUserList;
         }, function (error) {
            console.log('report services error....', error);
        });
 }else {
             $location.path('/userlogin');
         }


    });
   
   
    $scope.stockObject = {
        
        branchName : "Select sales Person",
        branchStocks:[]
    };
    
      $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });
    
    $scope.setBranch = function(salesPersonObj){
              var currentOrg=$scope.branchObj.Type;  

           if(salesPersonObj == currentOrg) { 
               $scope.branchObj.branchName = salesPersonObj;  
               $scope.salesPersionPartyId='';    
         }else {   
             $scope.branchObj.branchName = salesPersonObj.salesPersonName;
             $scope.salesPersionPartyId=salesPersonObj.partyId
         }
        
        
         $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
      
       StocksService.getBestProductData(choiceType,$scope.salesPersionPartyId).then(
        function(response){
            $scope.data = response.bestSaleUserList;
            $scope.search = {};
            $ionicLoading.hide();
            $scope.modal.hide();
        },function(error){
            $ionicLoading.hide();
            console.log('Error');
            console.log('error cant fetch branch stock');
        });
     };
    
    
    function showTimePeriodPopup(){
        $ionicPopup.show({
            title:'',
            templateUrl: 'templates/report-option-template.html',
            scope: $scope,
            buttons:[
                {
                    text: 'Cancel',
                    type: 'button-stable',
                    onTop: function (e) {
                        $scope.reportType.val ='Daily';
                    }
                },
                {
                    text:'ok',
                    type: 'button-positive',
                    onTap: function (e) {
                           choiceType=$scope.choice.val; StocksService.getBestProductData($scope.choice.val,$scope.salesPersionPartyId).then(function (response){
                                 $scope.data = response.bestSaleUserList;
                                 console.log('report Data',  $scope.data);
                            }, function (error){
                                 console.log('report services error....', response);
                                
                            });
                            $scope.reportType = $scope.choice;
                            
                        
                    }
                }
            ]
        });
    };

})











































