angular.module('services', [])

    .service('UserService', function () {

        //for the purpose of this example I will store user data on ionic local storage but you should save it on a database

        var pharmacyusers = {};

        var setUser = function (user_data) {
            
            if(user_data) {
            pharmacyusers.name = user_data[0].userFullName;
            pharmacyusers.orgId = user_data[0].organizationId;
            pharmacyusers.partyId = user_data[0].loginType.partyId;
            pharmacyusers.organizationName = user_data[0].currencyUomId.organizationName;
            pharmacyusers.organizationType = user_data[0].currencyUomId.organizationType;
            pharmacyusers.userId = user_data[0].userId;
            pharmacyusers.mailId = user_data[0].emailAddress;
            pharmacyusers.mobileNo = user_data[0].phoneNumber;
            pharmacyusers.gender = user_data[0].gender;
            pharmacyusers.ownclinic = user_data[0].externalUserId;
            }else {
                pharmacyusers = {};
            }
            
         };

        var getUser = function () {
             return pharmacyusers;
        };

        return {
            getUser: getUser,
            setUser: setUser
        };
    })
    .service('LoginService', function ($q, $http, UrlEndpoint) {
        this.loginAuth = function (info) {
            var deferred = $q.defer();
            console.log('enpoint url....', UrlEndpoint);
            var baseUrl = UrlEndpoint + 'SignInApp/UserLoginWebService?secureLoginId=' + info.username + '&securePassword=' + info.password;

            $http.get(baseUrl).then(function (response) {

                deferred.resolve(response.data);
            }, function (arguments) {
                deferred.reject(arguments);
            });
            return deferred.promise;
        }

    })
    .service('LogoutService', function ($q, $http, UrlEndpoint) {
        this.logout = function () {
            var deferred = $q.defer();
            console.log('enpoint url....', UrlEndpoint);
            var baseUrl = UrlEndpoint + 'SignInApp/logout';
             $http.get(baseUrl).then(function (response) {
                 deferred.resolve(response.data);
            }, function (arguments) {
                deferred.reject(arguments);
            });
            return deferred.promise;
        }

    })


// View stock Details for Inventory Screen
	.service('Inventory', function ($q, $http, $ionicLoading, UserService, UrlEndpoint) {
        this.getBranchList = function () {
            var org = UserService.getUser()
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
            $http.get(UrlEndpoint + 'rest/s1/ViewStock/activatedBranchList?organizationId=' + org.orgId).then(function (response) {

                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };

        this.getMainBranchStocksList = function () {

            var org = UserService.getUser();
            var deferred = $q.defer();

            $http.get(UrlEndpoint + 'rest/s1/ViewStock/stockDetailsBase?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };

        this.getBranchStocksList = function (branchInfoObj) {

            var deferred = $q.defer();
            $http.get(UrlEndpoint + 'rest/s1/ViewStock/stockDetailsBase?loginPartyId=' + branchInfoObj.partyId + '&loginOrgId=' + branchInfoObj.organisationId + '&loginUserId=' + branchInfoObj.userId).then(function (response) {

                deferred.resolve(response);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };



    })

    .service('StocksService', function ($q, $http, $ionicLoading, UserService, UrlEndpoint) {
     var org = UserService.getUser()
        this.getBranchList = function () {
           
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
             $http.get(UrlEndpoint + 'rest/s1/ViewStock/allSalesPerson?loginPartyId=' + org.partyId).then(function (response) {
                 deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };

        this.getMainBranchStocksList = function () {
            var org = UserService.getUser();
            var deferred = $q.defer();

                    $http.get(UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId).then(function (response) {

                         deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };
    
        this.getBranchStocksList = function (salesInfoObj) {

            var deferred = $q.defer();
    
                var org = UserService.getUser();
            $http.get(UrlEndpoint +'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId='+org.orgId+'&salesPerson='+salesInfoObj.partyId).then(function (response) {

                deferred.resolve(response);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };
        
       this.getBestProductData = function (selectedType,salesPersionPartyId) {
             var bestSalesurl='';
            if(salesPersionPartyId){
             if(selectedType == 'Daily') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId + '&salesPerson='+salesPersionPartyId
                
            }else if(selectedType == 'Weekly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId +'&timePeriod=Week'+'&salesPerson='+salesPersionPartyId
                
            }else if(selectedType == 'Monthly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId + '&timePeriod=Month'+'&salesPerson='+salesPersionPartyId
                
            }else {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId + '&timePeriod=Year'+'&salesPerson='+salesPersionPartyId
            }
            }else {
            
            if(selectedType == 'Daily') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId
                
            }else if(selectedType == 'Weekly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId +'&timePeriod=Week'
                
            }else if(selectedType == 'Monthly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId +'&timePeriod=Month'
                
            }else {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/bestSalesOrderProduct?loginOrgId=' + org.orgId +'&timePeriod=Year'
            }
            console.log('bestSalesurl' ,bestSalesurl);
            }
            var deferred = $q.defer();
            if(bestSalesurl) {
                  console.log('jsonUrl ', bestSalesurl); 
            $http.get(bestSalesurl).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            }
            return deferred.promise;
        };
    
    

    })
    .service('DashboardService', function ($q, $http, UserService, UrlEndpoint) {

        var org = UserService.getUser();
        this.getSalesData = function () {
            var org = UserService.getUser()
            var deferred = $q.defer();
             $http.get(UrlEndpoint + 'rest/s1/ViewStock/dashBordScreen?loginOrgId=' + org.orgId).then(function (response) {
                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;
        };

    })
    .service('SalesReportService', function ($q, $http, $ionicLoading, UserService, UrlEndpoint) {

        var org = UserService.getUser();
            this.getSalesReportData = function (selectedType,salesPartyId) {
           console.log('selectedType123', selectedType);
            var bestSalesurl='';
            if(salesPartyId){
             if(selectedType == 'Daily') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId+'&salesPartyId='+salesPartyId
                
            }else if(selectedType == 'Weekly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId+'&timePeriod=Week'+'&salesPartyId='+salesPartyId
                
            }else if(selectedType == 'Monthly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId+'&timePeriod=Month'+'&salesPartyId='+salesPartyId
                
            }else {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId+'&timePeriod=Year'+'&salesPartyId='+salesPartyId
            }
            }else {
            
            if(selectedType == 'Daily') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId
                
            }else if(selectedType == 'Weekly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId+'&timePeriod=Week'
                
            }else if(selectedType == 'Monthly') {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId+'&timePeriod=Month'
                
            }else {
                bestSalesurl=UrlEndpoint + 'rest/s1/ViewStock/salesReportStockRequest?loginOrgId=' + org.orgId+'&timePeriod=Year'
            }
            
            }
            var deferred = $q.defer();
            if(bestSalesurl) {
                  console.log('jsonUrl ', bestSalesurl); 
            $http.get(bestSalesurl).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            }
            return deferred.promise;
        };     
 

    })
 

    .service('UtilsService',function(){

         this.logger = function() {

                console.log('logging service....');
    };
        this.returnFormattedDate = function(rawDateObj) {

        var d = new Date(rawDateObj),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
    };

    this.returnFormattedDateForService = function(rawDateObj) {

        var d = new Date(rawDateObj),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
    };


    this.returnDay = function(day){

        var dayString = '';

        switch (day){

            case 0:
            dayString = 'Sunday';
            break;
            case 1:
            dayString = 'Monday';
            break;
            case 2:
            dayString = 'Tueday';
            break;
            case 3:
            dayString = 'Wednesday';
            break;
            case 4:
            dayString = 'Thursday';
            break;
            case 5:
            dayString = 'Friday';
            break;
            case 6:
            dayString = 'Saturday';
            break;
            default:
            dayString = '';
            break;
            
        }

        return dayString;
    };

    })
   





















