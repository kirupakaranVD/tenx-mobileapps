var app = angular.module('starter.signinpagecontroller', [])

//SignInCtrl
app.controller('SignInCtrl',function($scope,$base64,$rootScope,$state,$ionicLoading,LoginService,UserService,$cordovaNetwork,$localstorage,$rootScope) {

	$scope.remember='';
    if($localstorage.getObject('username')){
         $scope.user = {
         username : [$localstorage.getObject('username')],
         password : [$localstorage.getObject('password')]
    }; 
    }else {
        $scope.user = {};  
    }
  
    $scope.sessionObj='';


    
    $scope.signIn=function(userInfo){
console.log('error....');
//if(navigator.connection.type === "none"){
//    
//    alert("No Internet Connectivity");
//    
//    
//}else{
         var encodedusername = $base64.encode(userInfo.username);
          var encodedpwd = $base64.encode(userInfo.password);
    var decodedusername = $base64.decode(encodedusername);
    var decodedpwd = $base64.decode(encodedpwd);
   
             $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
        LoginService.loginAuth(userInfo)
            .then(function(response){
            console.log('success....',response);
            switch(response[0].errorCode)
            {
                    
                case "401" :
                    $ionicLoading.hide();
                    alert(response[0].errorMsg);
                    
                break;
                    
                default :
                 $ionicLoading.hide();
                 UserService.setUser(response);
                 UserService.getUser();
				 $scope.sessionObj = UserService.getUser().userId;
                $state.go('menu.home');
				
				if($rootScope.rememberMe.checked) {
					$localstorage.setObject('username',$scope.user.username);
					$localstorage.setObject('password',$scope.user.password);
				}  

            }
            $scope.user = {};
            
        },function(error){
            $ionicLoading.hide();   
            alert("Invalid Credentials , please try again");
            $scope.user = {};
            
             console.log(error);
            
        })

    //}
    };
	
	$scope.rememberMe=function(remember){
		 if(remember) {
			$rootScope.rememberMe ={ text: "Remember Me", checked: true }; 
		}else {
			$rootScope.rememberMe ={ text: "Remember Me", checked: false };
		}
	};
    
})
    .controller('NavController', function($scope, $ionicSideMenuDelegate) {
      $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
      };
    })
.controller('MenuController',function(){
    
})
.controller('HomePageController',function(){
    
})
.controller('SalesPageController',function(){
    
})
.controller('StocksPageController',function(){
    
})
.controller('BestPageController',function(){
    
});
