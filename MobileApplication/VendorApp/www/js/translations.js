var translations = {
	"en": {
        "Username":"Username",
        "Password":"Password",
        "login":"Login",
        "Appoinment":"Appoinment",
        "Roster":"Roster",
        "Appoinments":"Appoinments",
        "First":"First",
        "Last":"Last",
        "Hospital List":"Hospital List",
		"Sales": "Sales",
		"Stocks": "Stocks",
		"Best": "Best",
        "Home":"Home",
        "Sales Report":"Sales Report",
        "Stock Details":"Stock Details",
        "Best Products":"Best Products",
        "Language Setting":"Language Setting",
        "Logout":"Logout",
        "Sunday":"Sunday",
        "Monday":"Monday",
        "Tuesday":"Tuesday",
        "Wednesday":"Wednesday",
        "Thursday":"Thursday",
        "Friday":"Friday",
        "Saturday":"Saturday",
        "Total billing":"Total billing",
        "No of Invoices":"No of Invoices",
        "click to choose an option" : "click to choose an option",
        "Daily Report":"Daily Report",
        "Weekly Report":"Weekly Report",
        "Monthly Report":"Monthly Report",
        "Yearly Report":"Yearly Report",
        "Choose Report type":"Choose Report type",
        "Daily":"Daily",
        "Weekly":"Weekly",
        "Monthly":"Monthly",
        "Yearly":"Yearly",
		"Profile":"Profile",
        "Appointment":"Appointment",
        "Roster":"Roster",
        "First":"First",
        "Last":"Last",
        "Sun":"Sun",
        "Mon":"Mon",
        "Tue":"Tue",
        "Wed":"Wed",
        "Thu":"Thu",
        "Fri":"Fri",
        "Sat":"Sat",
        "All":"All",
        "Sesh 1":"Sesh 1",
        "Sesh 2":"Sesh 2",
        "Sesh 3":"Sesh 3",
        "Sesh 4":"Sesh 4",
        "Prev":"Prev",
        "Next":"Next",
        "Save":"Save",
        "January":"January",
        "February":"February",
        "March":"March",
        "April":"April",
        "May":"May",
        "June":"June",
        "July":"July",
        "August":"August",
        "September":"September",
        "October":"October",
        "November":"November",
        "December":"December",
		"pick date":"pick date"
	},
	"es": {
        "Username":"Nombre de usuario",
        "Password":"contraseña",
        "login":"Iniciar sesión",
        "Appoinment":"Appoinment",
        "Roster":"Roster",
        "Appoinments":"Appoinments",
        "First":"First",
        "Last":"Last",
        "Hospital List":"Hospital List",
		"Sales": "Ventas",
		"Stocks": "Valores",
		"Best": "Mejor",
        "Home":"Casa",
        "Sales Report":"Reporte de ventas",
        "Stock Details":"Detalles del stock",
        "Best Products":"Mejor Producto",
        "Language Setting":"configuración de idioma",
        "Logout":"Cerrar sesión",
        "Sunday":"domingo",
        "Monday":"lunes",
        "Tuesday":"martes",
        "Wednesday":"miércoles",
        "Thursday":"jueves",
        "Friday":"viernes",
        "Saturday":"sábado",
        "Total billing":"Facturación total",
        "No of Invoices":"Número de factura",
        "click to choose an option":"Haga clic para elegir una opción",
        "Daily Report":"Reporte diario",
        "Weekly Report":"Reporte semanal",
        "Monthly Report":"Reporte mensual",
        "Yearly Report":"Informe Anual",
        "Choose Report type":"Seleccione Tipo de informe",
        "Daily":"Diariamente",
        "Weekly":"Semanal",
        "Monthly":"Mensual",
        "Yearly":"Anual",
		"Profile":"Perfil",
        "Appointment":"Cita",
        "Roster":"Lista",
        "First":"Inicio",
        "Last":"Última",
        "Sunday":"Domingo",
        "Monday":"Lunes",
        "Tuesday":"Martes",
        "Wednesday":"Miércoles",
        "Thursday":"Jueves",
        "Friday	":"Viernes",
        "Saturday":"Sábado",
        "Sun":"Do",
        "Mon":"Lun",
        "Tue":"Mar",
        "Wed":"Mier",
        "Thu":"Jue",
        "Fri":"Vie",
        "Sat":"Sab",
        "All":"Todas",
        "Sesh 1":"Sesh 1",
        "Sesh 2":"Sesh 2",
        "Sesh 3":"Sesh 3",
        "Sesh 4":"Sesh 4",
        "Prev":"Regresar",
        "Next":"Siguiente",
        "Save":"Guardar",
        "January":"Enero",
        "February":"Febrero",
        "March":"Mazro",
        "April":"Abril",
        "May":"Mayo",
        "June":"Junio",
        "July":"Julio",
        "August":"Agosto",
        "September":"Septiembre",
        "October":"Octubre",
        "November":"Noviembre",
        "December":"Diciembte",
		"pick date":"Fecha de selección",
		"Sales Order Report":"Informe de órdenes de venta",
		"Inventory":"Inventario",
		"Best Sellers":"Los más vendidos",
		"No Data Available":"Datos no disponibles",
		"No of SO":"Número de orden de compra",
		"SO Value":"Valor del pedido de venta",
		"Cancel":"Cancelar"
        

	}
}

































