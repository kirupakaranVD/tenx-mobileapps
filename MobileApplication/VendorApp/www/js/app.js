angular.module('starter', ['ionic','base64','pascalprecht.translate','ngCordova', 'nvd3', 'starter.signinpagecontroller',
                           'starter.menucontroller',
                           'starter.homepagecontroller', 'starter.salespagecontroller', 'starter.stockspagecontroller', 'starter.bestpagecontroller', 'starter.webservicesendpoints','starter.profilepagecontroller', 'services','ionic.utils'])

    .run(function ($ionicPlatform,$state,$ionicHistory,$localstorage,$rootScope) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if($localstorage.getObject('username') !='') {
                 $rootScope.rememberMe ={ text: "Remember Me", checked: true };
            }else {
                 $rootScope.rememberMe ={ text: "Remember Me", checked: false };
            }
            
            
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider,$translateProvider) {
    
        for(lang in translations){
        
           $translateProvider.translations(lang, translations[lang]);
        }
        $translateProvider.preferredLanguage('es');
        $httpProvider.defaults.withCredentials = true;
        $stateProvider
            .state('userlogin', {
                url: '/userlogin',
                templateUrl: 'templates/userlogin.html',
                controller: 'SignInCtrl'
            })
            .state('menu', {
                url: '/menu',
                templateUrl: 'templates/menu.html',
                abstract: true,
                controller: 'MenuController'
            })
            .state('menu.home', {
                url: '/home',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-home.html',
                        controller: 'HomePageController'
                    }
                }
            })
            .state('menu.profile', {
                url: '/profile',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-profile.html',
                         controller: 'ProfilePageController'
                     }
                }
            })    
            .state('menu.sales', {
                url: '/sales',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-sales.html',
                        controller: 'SalesPageController'
                    }
                }
            })
            .state('menu.stocks', {
                url: '/stocks',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-stocks.html',
                        controller: 'StocksPageController'
                    }
                }
            })
            .state('menu.best', {
                url: '/best',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-best.html',
                        controller: 'BestPageController'
                    }
                }
            })
            .state('menu.test', {
                url: '/test',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-test.html',
                        controller: 'TestPageController'
                    }
                }
            })

        $urlRouterProvider.otherwise('/userlogin');
    });
