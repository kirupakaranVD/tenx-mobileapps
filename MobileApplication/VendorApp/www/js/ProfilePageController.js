var app=angular.module('starter.profilepagecontroller',[])

app.controller('ProfilePageController',function($scope,UserService,$location){
    
     $scope.$on('$ionicView.beforeEnter', function(){
          var  userName=UserService.getUser();
         if(userName.userId) {
             $scope.currentOrgName = userName.type;
             $scope.currentOrgiName = userName.organizationName;
             $scope.currentUserName = userName.name;
             $scope.currentMailId = userName.mailId;
             $scope.currentContactNo = userName.mobileNo;
             $scope.currentIdentification = userName.gender; 
         }else {
             $location.path('/userlogin');
         }
      });
 })