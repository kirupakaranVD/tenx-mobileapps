var app = angular.module('starter.salespagecontroller', []);

app.controller('SalesPageController', function ($scope, $ionicModal, $ionicPopup, $ionicLoading, UserService, StocksService, SalesReportService,$translate,$location) {

    console.log();
    $scope.branchObj = {
        branchName:'',
        branchType:'',
        orgId:'',
        partyId:'',
        userId:''
    };
    $scope.listofpharmacies = {
        branchDetailsList: []
    };
      $scope.barChartData=[];
    $scope.choice = {
        val: 'Daily'
    };
    $scope.reportType = {
        val: 'Daily'
    };
     var choiceType='Daily';
    $scope.salesListInVendorPr = {
        salesDetailsList: []
    };
    $scope.showSortPopup = showSortPopup;
    $scope.totalSalesDetail = {};
    $scope.dataDR = [
        {
            values: []

                }
            ];

    $scope.dataWR = [
        {
            values: [

                ]
            }
        ];
    $scope.dataMR = [
        {
            values: [

                ]
            }
        ];
    $scope.dataYR = [
        {
            values: [
                ]
            }
        ];

    var formatValue = d3.format(",.2s");
    $scope.optionsDR = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            width:500,
            margin: {
                top: 20,
                right: 0,
                bottom: 50,
                left: 55
            },
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.value;
            },
            showValues: false,

            duration: 500,
            xAxis: {
                axisLabel: 'Hours'
            },
            yAxis: {
                axisLabel: 'Sales (Q)',
                axisLabelDistance: -6,
             
            }
        }
    };
    $scope.optionsWR = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            margin: {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.value;
            },
            showValues: false,

            duration: 500,
            xAxis: {
                axisLabel: 'Week'
            },
            yAxis: {
                axisLabel: 'Sales (Q)',
                axisLabelDistance: -6,
                tickFormat: function (d) {
                    return formatValue(d)
                }

            }
        }
    };



$scope.optionsMR = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            width:600,
            margin: {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.value;
            },
            showValues: false,

            duration: 500,
            xAxis: {
                axisLabel: 'Days'
            },
            yAxis: {
                axisLabel: 'Sales (Q)',
                axisLabelDistance: -6,
                tickFormat: function (d) {
                    return formatValue(d)
                }

            }
        }
    };


    $scope.optionsYR = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            margin: {
                top: 20,
                right: 0,
                bottom: 50,
                left: 60
            },
            x: function (d) {
                return d.label;
            },
            y: function (d) {
                return d.value;
            },
            showValues: false,
            duration: 500,
            xAxis: {
                axisLabel: 'Months',
            },
            yAxis: {

                axisLabel: 'Sales (Q)',
                axisLabelDistance: -6,
                tickFormat: function (d) {
                    return formatValue(d)
                }

            }
        }
    };


    $scope.$on('$ionicView.beforeEnter', function () {
           var pharmacyName = UserService.getUser();
        if(pharmacyName.userId) {
        $scope.branchObj.branchName = pharmacyName.name;
        $scope.branchObj.branchType = pharmacyName.type;
       $scope.branchObj.Type = pharmacyName.name;
         StocksService.getBranchList().then(function(response){
            $scope.salesListInVendorPr.salesDetailsList = response.salesPersonlist1;
            $ionicLoading.hide();
        },function(error){
            console.log('error');
            $ionicLoading.hide();
        });

      showSalesReportData(choiceType,$scope.salesPersionPartyId)
    }else {
             $location.path('/userlogin');
         }
        
    });
   
     $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });
    
  $scope.setBranch = function(salesPersonObj){
            var currentOrg=$scope.branchObj.Type;  
            if(salesPersonObj == currentOrg) { 
               $scope.branchObj.branchName = salesPersonObj;  
               $scope.salesPersionPartyId='';    
         }else {   
             $scope.branchObj.branchName = salesPersonObj.salesPersonName;
             $scope.salesPersionPartyId=salesPersonObj.partyId
         }
        
        
         $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
      showSalesReportData(choiceType,$scope.salesPersionPartyId)
     $ionicLoading.hide();
            $scope.modal.hide();
     };
   

    function showSortPopup() {
        $ionicPopup.show({
            title: '',
            templateUrl: 'templates/report-option-template.html',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel',
                    type: 'button-stable',
                    onTap: function (e) {
                        $scope.reportType.val = 'Daily';
                        
                    }
                        },
                {
                    text: 'Ok',
                    type: 'button-positive',
                    onTap: function (e) {
                        choiceType=$scope.choice.val; 
                       showSalesReportData($scope.choice.val,$scope.salesPersionPartyId)
                     
                     }
                        }
                    ]
         });

     };
    
    function showSalesReportData(reportType,selectedpartyId) {
         SalesReportService.getSalesReportData(reportType,selectedpartyId).then(function (response) {
                               if (reportType == 'Daily') {  
                                   var dailyReporthistory = [];
                                   $scope.dataDR[0].values = createReortData(response.salesHistoryList, 'Daily');
                                 }else if (reportType == 'Weekly') {
                                     $scope.dataWR[0].values = createReortData(response.salesHistoryList, 'Weekly');
                                 
                                 }else if (reportType == 'Monthly') {
                                      $scope.dataMR[0].values = createReortData(response.salesHistoryList, 'Monthly');
                                 
                                 }else if (reportType== 'Yearly') { 
                                     $scope.dataYR[0].values = createReortData(response.salesHistoryList, 'Yearly');
                                  }
                                 $scope.barChartData=response.salesHistoryList;    
                             }, function (error) {
                                console.log('report services error....', response);
                            });
                            $scope.reportType = $scope.choice;
        
    }
     
})



var createReortData = function (res, reportType) {

    var reportDataArray = [];

    for (var i = 0; i < res.length; i++) {

        var reportDataObj = {};
        angular.forEach(res[i], function (value, key) {

            if (reportType == 'Daily') {
                switch (key) {
                    case "Hour":
                         reportDataObj.label = value;
                         break;
                    case "requiredQuantity":
                        reportDataObj.value = parseInt(value);
                        break;
                    default:
                         break;
                }

            } else if (reportType == 'Weekly') {
                switch (key) {
                    case "Day":
                         reportDataObj.label = value;
                         break;
                    case "requiredQuantity":
                        reportDataObj.value = parseInt(value);
                         break;
                    default:

                        break;
                }

            } else if (reportType == 'Monthly') {
                switch (key) {
                    case "Day":
                         reportDataObj.label = value;
                         break;
                    case "requiredQuantity":
                        reportDataObj.value = parseInt(value);
                         break;
                    default:

                        break;
                }

            } else if (reportType == 'Yearly') {
                switch (key) {
                    case "Month":
                         reportDataObj.label = value;
                         break;
                    case "requiredQuantity":
                         reportDataObj.value = parseInt(value);
                        break;
                    default:
                         break;
                }

            }

        })
        reportDataArray.push(reportDataObj);

    }
      return reportDataArray;

};




