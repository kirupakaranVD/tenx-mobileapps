var app = angular.module('starter.homepagecontroller', [])

//SignInCtrl
app.controller('HomePageController',function($scope,$state,UtilsService,$ionicPlatform,$ionicLoading,$ionicPopup,DashboardService,UserService,$location) {
    
  $scope.salesDay = {
      date:"",
      day:""
  };
    $ionicPlatform.onHardwareBackButton(function() {
    if($state.current.name == 'menu.home'){
        
            $ionicPlatform.registerBackButtonAction(function(event) {
    if (true) {
        // your check here
         if($state.current.name == 'menu.home'){
            $ionicPopup.confirm({
        title: 'Warning',
        template: 'Are you sure you want to exit?'
      }).then(function(res) {
        if (res) {
          ionic.Platform.exitApp();
        }
      })
        }
      
    }
  }, 100);
    }
        
     });
    
    $ionicPlatform.registerBackButtonAction(function(event) {
    if (true) {
        // your check here
         if($state.current.name == 'menu.home'){
            $ionicPopup.confirm({
        title: 'System warning',
        template: 'are you sure you want to exit?'
      }).then(function(res) {
        if (res) {
          ionic.Platform.exitApp();
        }
      })
        }
      
    }
  }, 100);
    
    
    
    
    
    $scope.$on('$ionicView.beforeEnter', function(){
         var  userName=UserService.getUser();
         if(userName.userId) {
        $scope.salesDay.date = UtilsService.returnFormattedDate(new Date());
        $scope.salesDay.day = UtilsService.returnDay(new Date().getDay());
        DashboardService.getSalesData().then(function(response){
            $scope.salesRecord = response.dashBordList[0];
         },function(error){
                 console.log('Dashboard service error');
         })
    }else {
             $location.path('/userlogin');
         }
    });

})