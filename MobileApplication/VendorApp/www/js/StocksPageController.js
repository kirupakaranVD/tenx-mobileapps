var app = angular.module('starter.stockspagecontroller', [])

 app.controller('StocksPageController',function($scope,$timeout,$ionicModal,$ionicLoading,UserService,StocksService,$ionicPlatform,$ionicScrollDelegate,Inventory,$location) {
 

    $scope.pharmacies = [];
    $scope.stocksDetails = {
        
        stocksListArray : []
    };
    $scope.listofpharmacies = {
        branchDetailsList : []
    };
    $scope.$on('$ionicView.beforeEnter', function(){
        
       var pharmacyName = UserService.getUser();
        
        if(pharmacyName.userId) {
        $scope.stockObject.branchName = pharmacyName.name;
        Inventory.getMainBranchStocksList().then(function(response){
             $scope.stocksDetails.stocksListArray = response.salesHistoryList;
          },function(error){
             console.log("can't fetch stock list..try again...");
        });
         }else {
             $location.path('/userlogin');
         }
        
    });

    $scope.search = {};
    $scope.query = {};
    $scope.stockObject = {
        
        branchName : "Select Pharmacy",
        branchStocks:[]
    };
    
      $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
    
    $scope.setBranch = function(pharmacyObj){
        
        console.log('branchIndex....',pharmacyObj);
        
        $scope.stockObject.branchName = pharmacyObj.organizationName;
        var branchDetail = {

            organisationId: pharmacyObj.orginatationId,
            partyId: pharmacyObj.partyId,
            userId: pharmacyObj.userId
        };
         $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
        Inventory.getBranchStocksList(branchDetail).then(
        function(response){
           
             $scope.stocksDetails.stocksListArray = response.data.salesHistoryList;
            console.log('branch stock...',$scope.stockObject.branchStocks);
            $scope.search = {};
            $ionicLoading.hide();
            $scope.modal.hide();
        },function(error){
            $ionicLoading.hide();
            $scope.modal.hide();
            alert("Error. please try again");
            console.log('error cant fetch branch stock');
        });
        
        
    };

})