var app = angular.module('starter.homepagecontroller', [])

//SignInCtrl
app.controller('HomePageController',function($scope,$ionicLoading,$ionicPlatform,UtilsService,HomepageService,$location,UserService) {

$scope.doctorSchedule = {
            'date':'',
            'day':'',
            'startTime':'00:00',
            'endTime':'00:00',
            'totalAppointments':'0'
        };

$scope.rosters = [];

$ionicPlatform.onHardwareBackButton(function() {
     event.preventDefault();
     event.stopPropagation();
     alert('Backbutton Pressed');
  });

$ionicPlatform.registerBackButtonAction(function(event) {
    if (true) { // your check here
      $ionicPopup.confirm({
        title: 'System warning',
        template: 'are you sure you want to exit?'
      }).then(function(res) {
        if (res) {
          ionic.Platform.exitApp();
        }
      })
    }
  }, 100);
$scope.$on('$ionicView.beforeEnter', function(){
        
     var  userName=UserService.getUser();
        if(userName.userId) {
    
        $scope.doctorSchedule.date = UtilsService.returnFormattedDate(new Date());
        $scope.doctorSchedule.day = UtilsService.returnDay(new Date().getDay());
        var dateObj = UtilsService.returnFormattedDateForService(new Date());
        HomepageService.getScheduleData(dateObj).then(function(response){
            $scope.doctorSchedule.totalAppointments = response.totalAppointmentAvailable[0].countOfApportment;
            
        },function(error){
           
        });
        HomepageService.getTimeSlot($scope.doctorSchedule.day).then(function(response){
            console.log(response.doctorsClinicFinalList[0]);
            $scope.doctorSchedule.startTime = response.doctorsClinicFinalList[0].doctorStartTime;
            $scope.doctorSchedule.endTime = response.doctorsClinicFinalList[0].doctorEndingTime;
        },function(error){
           
        });
        HomepageService.getAppointments($scope.doctorSchedule.day).then(function(response){
            console.log(response.doctorsClinicFinalList);
           $scope.rosters = response.doctorsClinicFinalList;
        },function(error){
            
        });
    
        }else {
             $location.path('/userlogin');
        }
    
    });
    
})