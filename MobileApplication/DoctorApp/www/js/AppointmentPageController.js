var app = angular.module('starter.appointmentpagecontroller', ['pickadate']);

app.controller('AppointmentPageController', function ($scope, $ionicModal, $ionicPopup, $ionicLoading, $cordovaDatePicker,UserService, AppointmentService,ClinicService, $translate,UtilsService,$location) {

    
    $scope.sessionArray = ['AllSession', 'Session1', 'Session2', 'Session3','Session4'];
    $scope.ownclinic = {};
    $scope.childCount;
    $scope.formattedDate;
    $scope.listofclinics = {
        clinicDetailList: []
    };
    $scope.appointmentFilter={
        'query':''
    };
    $scope.clinicObj = {
        'clinicName':'Select Clinic'
    };
    $scope.selectedClinicObj = {};
    $scope.appointments = [];
    $scope.sessionObj = {
        'value':'All Session'
    };
     $scope.choice = {
        val: 'All'
    };
    $ionicModal.fromTemplateUrl('templates/modal.html', {
         scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
     });

    $scope.showSessionPopup = showSessionPopup;

    $scope.showClinicModal = function () {

        $scope.modal.show();
    };


    $scope.getSessionCode = function(sessionObj){
         var retVal = "";
        switch(sessionObj){
            case 'Session1':
            retVal='S1';
            break;
            case 'Session2':
            retVal='S2';
            break;
            case 'Session3':
            retVal='S3';
            break;
            case 'Session4':
            retVal='S4';
            break;
            default :
            retVal = "";
            break;
        }

        return retVal;
    };

 
    $scope.$on('$ionicView.afterEnter', function () {
        var  userName=UserService.getUser();
        if(userName.userId) {
            
      
       highlightCurrentDay($scope.sessionArray);
       var formattedDate = UtilsService.returnFormattedDate(new Date());
        $scope.formattedDate = formattedDate;
         var obj_clinic = UserService.getUser();
        console.log(obj_clinic)
        $scope.ownclinic.name = obj_clinic.ownclinic;
         $scope.clinicObj.clinicName = obj_clinic.ownclinic;
       var formattedDate = UtilsService.returnFormattedDateForService(new Date());  
        getAppointmentDateWise(formattedDate);
         }else {
              $location.path('/userlogin');
         } 
    });

    $scope.setOwnClinic = function(){
              $scope.clinicObj.clinicName=$scope.ownclinic.name;
              AppointmentService.getOwnClinicAppointment().then(function (response) {
             $scope.appointments = response.bookAppointmentOpen;
            $scope.appointmentFilter.query = "";
            $scope.sessionObj.value = 'All Session';

            $scope.modal.hide();
        }, function (error) {
            console.log('error');
            $scope.modal.hide();
            $ionicLoading.hide();
        });
      };
    
    function getAppointmentDateWise(selectedDate) {
        
          AppointmentService.getOwnClinicAppointment(selectedDate).then(function (response) {
             $scope.appointments = response.bookAppointmentOpen;
            ClinicService.getClinicList().then(function(response){
                 $scope.listofclinics.clinicDetailList=response.doctorsClinicList;
            },function(error){
                console.log('clinic list error....',error);
            });
            $ionicLoading.hide();
        }, function (error) {
            console.log('error');
            $ionicLoading.hide();
        });

        
    }
    
    
    $scope.setClinic = function(clinicData){
            console.log(clinicData);
              
              AppointmentService.getClinicAppointment(clinicData).then(function (response) {
            
            $scope.appointments = response.bookAppointmentOpen;
             $scope.appointmentFilter.query = "";
            if(clinicData.organizationName && clinicData.organizationId)
            {
                $scope.selectedClinicObj = clinicData;
             }
            if(clinicData.organizationName){
                $scope.clinicObj.clinicName=clinicData.organizationName;
            }else{
                $scope.clinicObj.clinicName="Pharmacy";
            }
            
            $scope.modal.hide();
        }, function (error) {
            console.log('error');
            $scope.modal.hide();
            $ionicLoading.hide();
        });


    };

    function showSessionPopup() {
        console.log($scope.branchObj);
        
            $ionicPopup.show({
            title: '',
            templateUrl: 'templates/report-session-template.html',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel',
                    type: 'button-stable',
                    onTap: function (e) {
                       
                        console.log($scope.sessionObj.value);
                    }
                        },
                {
                    text: 'Ok',
                    type: 'button-balanced',
                    onTap: function (e) {
                        console.log($scope.choice);
                         $scope.childCount = angular.element(document.querySelector("#appointment-container"))[0].childElementCount;
                         console.log('previoussibling',angular.element(document.querySelector("#appointment-container"))[0].childElementCount)
                        console.log('total childcount....',$scope.childCount);

                        return $scope.choice;
  
                    }

                    }
                    ]
         });
     };
   
    $ionicModal.fromTemplateUrl('templates/datemodal.html', 
        function(modal) {
            $scope.datemodal = modal;
        },
        {
         scope: $scope, 
         animation: 'slide-in-up'
        
        }
    );
    $scope.opendateModal = function() {
      $scope.datemodal.show();
     };
    $scope.closedateModal = function(modal) {
         getAppointmentDateWise(modal);
      $scope.datemodal.hide();
      $scope.datepicker = modal;
    };
 

    function formatDate(dateObj) {
    var d = new Date(dateObj),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};


    function getChildClinicAppointmentFromDate(clinicData,dateOfAppointment){
          AppointmentService.getChildClinicAppointmentFromDate(clinicData,dateOfAppointment).then(function(response){
             $scope.appointments = response.bookAppointmentOpen;
         },function(error){
            alert('can\'t fetch data, please try again');
        })
    };
    function getMasterClinicAppointmentFromDate(dateOfAppointment){
         AppointmentService.getMasterClinicAppointmentFromDate(dateOfAppointment).then(function(response){
            $scope.appointments = response.bookAppointmentOpen;
          },function(error){
            alert('can\'t fetch data, please try again');
        })
    };
 
    $scope.selectThisSession = function(selectedSession){
         deselectAllDay($scope.sessionArray);
        highlightTabIndex(selectedSession);
     };

    function highlightCurrentDay(today){
         for(var i=0 ; i<today.length;i++){
             if(today[i] == 'AllSession')
            {
                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#4CB050";
                elem[0].style.color = "#fff";
                $scope.appointmentFilter.query = "";

            }else{

                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#fff";
                elem[0].style.color = "#000";
            }

        }
    };

    function deselectAllDay(today){
         for(var i=0 ; i<today.length;i++){
                 var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#fff";
                elem[0].style.color = "#000";
         }
    };

    function highlightTabIndex(dayName){
          switch(dayName){
            case 'AllSession':
            var elem = angular.element(document.getElementById("AllSession"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "";
            break;
            case 'Session1':
            var elem = angular.element(document.getElementById("Session1"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session1";
            break;
            case 'Session2':
            var elem = angular.element(document.getElementById("Session2"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session2";
            break;
            case 'Session3':
            var elem = angular.element(document.getElementById("Session3"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session3";
            break;
            case 'Session4':
            var elem = angular.element(document.getElementById("Session4"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session4";
            break;
            default:
            break;
        }


    };


})