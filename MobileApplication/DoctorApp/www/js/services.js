angular.module('services', [])

    .service('UserService', function () {

        //for the purpose of this example I will store user data on ionic local storage but you should save it on a database

        var pharmacyusers = {};

        var setUser = function (user_data) {

          if(user_data) {
            pharmacyusers.type = user_data[0].loginType.roleTypeId;
            pharmacyusers.name = user_data[0].userFullName;
            pharmacyusers.orgId = user_data[0].organizationId;
            pharmacyusers.partyId = user_data[0].loginType.partyId;
            pharmacyusers.organizationName = user_data[0].currencyUomId.organizationName;
            pharmacyusers.organizationType = user_data[0].currencyUomId.organizationType;
            pharmacyusers.userId = user_data[0].userId;
			pharmacyusers.mailId = user_data[0].emailAddress;
            pharmacyusers.mobileNo = user_data[0].phoneNumber;
            pharmacyusers.gender = user_data[0].gender;
            pharmacyusers.ownclinic = user_data[0].externalUserId;
            
            console.log('user type...', pharmacyusers.type);
                        }else {
                            pharmacyusers={};
                        }
        };

        var getUser = function () {
           
            return pharmacyusers;
        };

        return {
            getUser: getUser,
            setUser: setUser
        };
    })
    .service('LoginService', function ($q, $http, UrlEndpoint) {
        this.loginAuth = function (info) {
            var deferred = $q.defer();
            console.log('enpoint url....', UrlEndpoint);
            var baseUrl = UrlEndpoint + 'SignInApp/UserLoginWebService?secureLoginId=' + info.username + '&securePassword=' + info.password;

            $http.get(baseUrl).then(function (response) {

                deferred.resolve(response.data);
            }, function (arguments) {
                deferred.reject(arguments);
            });
            return deferred.promise;
        }

    })
    .service('LogoutService', function ($q, $http, UrlEndpoint) {
        this.logout = function () {
            var deferred = $q.defer();
            console.log('enpoint url....', UrlEndpoint);
            var baseUrl = UrlEndpoint + 'SignInApp/logout';

            $http.get(baseUrl).then(function (response) {

                deferred.resolve(response.data);
            }, function (arguments) {
                deferred.reject(arguments);
            });
            return deferred.promise;
        }

    })
 .service('ClinicService', function ($q, $http, $ionicLoading, UserService, UrlEndpoint) {
        this.getClinicList = function () {
            var org = UserService.getUser()
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
            $http.get(UrlEndpoint + 'rest/s1/AppointmentHistory/clinicListMaptoDoctor?loginPartyId=' + org.partyId + '&loginOrgId='+ org.orgId +'&loginUserId='+ org.userId).then(function (response) {

                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };

        this.getMainBranchStocksList = function () {

            var org = UserService.getUser();
            var deferred = $q.defer();

            $http.get(UrlEndpoint + 'rest/s1/ViewStock/stockDetailsBase?loginPartyId=' + org.partyId + '&loginOrgId=' + org.orgId + '&loginUserId=' + org.userId).then(function (response) {

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };

        this.getBranchStocksList = function (branchInfoObj) {

            var deferred = $q.defer();
            $http.get(UrlEndpoint + 'rest/s1/ViewStock/stockDetailsBase?loginPartyId=' + branchInfoObj.partyId + '&loginOrgId=' + branchInfoObj.organisationId + '&loginUserId=' + branchInfoObj.userId).then(function (response) {

                deferred.resolve(response);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;


        };



    })
    .service('HomepageService', function ($q, $http, UserService, UrlEndpoint,UtilsService) {

        var org = UserService.getUser();
        console.log(org)
        this.getScheduleData = function (dateObj) {
            var deferred = $q.defer();
            
            $http.get(UrlEndpoint + 'rest/s1/AppointmentHistory/appointmentForDay?loginPartyId=' + org.partyId +'&loginOrgId=' + org.orgId +'&loginUserId='+ org.userId +'&fromDateApp='+dateObj).then(function (response) {
          

                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;
        };
      
        this.getTimeSlot = function (dayObj) {
            var deferred = $q.defer();
            var formattedDate = UtilsService.returnFormattedDateForService(new Date());
            $http.get(UrlEndpoint + 'rest/s1/AppointmentHistory/doctorStartTimeEndTime?loginPartyId=' + org.partyId +'&loginOrgId=' + org.orgId +'&loginUserId='+ org.userId +'&fromDateApp='+formattedDate).then(function (response) {
                
                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;
        };
        this.getAppointments = function (dayObj) {
            var deferred = $q.defer();

            $http.get(UrlEndpoint + 'rest/s1/AppointmentHistory/dashboardHomeDoctor?loginPartyId=' + org.partyId +'&loginOrgId=' + org.orgId +'&loginUserId='+ org.userId +'&weakdaysDay='+dayObj).then(function (response) {
                
                deferred.resolve(response.data);

            }, function () {
                deferred.reject(arguments);
            });
            return deferred.promise;
        };

    })
    .service('AppointmentService', function ($q, $http, $ionicLoading, UserService, UrlEndpoint,UtilsService) {
    
        
        this.getOwnClinicAppointment = function (selectedDate) {
            var org = UserService.getUser();
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
           
            var url=UrlEndpoint + 'rest/s1/AppointmentHistory/bookAppointments?loginPartyId=' + org.partyId +'&loginOrgId=' + org.orgId +'&loginUserId='+ org.userId +'&fromDateApp=' + selectedDate;
            $http.get(url).then(function (response) {
               
                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };

        this.getClinicAppointment = function (clinicobj) {
            var org = UserService.getUser();
            console.log(clinicobj.organizationId);
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
             var formattedDate = UtilsService.returnFormattedDateForService(new Date());
            
            $http.get(UrlEndpoint +'rest/s1/AppointmentHistory/bookAppointments?loginPartyId='+org.partyId+'&loginOrgId='+ org.orgId+'&loginUserId='+org.userId+'&switchOrgId='+clinicobj.organizationId+'&fromDateApp=' +formattedDate).then(function (response) {

                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };
        this.getMasterClinicAppointmentFromDate = function (dateObj) {
            var org = UserService.getUser();
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
            $http.get(UrlEndpoint +'rest/s1/AppointmentHistory/bookAppointments?loginPartyId='+org.partyId+'&loginOrgId='+ org.orgId+'&loginUserId='+org.userId+'&fromDateApp='+dateObj).then(function (response) {

                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };
        this.getChildClinicAppointmentFromDate = function (switchorgid,dateObj) {
            var org = UserService.getUser();
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
            $http.get(UrlEndpoint +'rest/s1/AppointmentHistory/bookAppointments?loginPartyId='+org.partyId+'&loginOrgId='+ org.orgId+'&loginUserId='+org.userId+'&switchOrgId='+switchorgid+'&fromDateApp='+dateObj).then(function (response) {

                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };

        
    })
    .service('RosterService', function ($q, $http,$ionicLoading,UserService, UrlEndpoint) {

        var org = UserService.getUser();
        this.getRosterData = function (dayObj) {
            var deferred = $q.defer();
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 500
            });
            $http.get(UrlEndpoint + 'rest/s1/AppointmentHistory/dashboardHomeDoctor?loginPartyId=' + org.partyId +'&loginOrgId=' + org.orgId +'&loginUserId='+ org.userId +'&weakdaysDay='+dayObj).then(function (response) {
                
                deferred.resolve(response.data);
                $ionicLoading.hide();

            }, function () {
                deferred.reject(arguments);
                $ionicLoading.hide();
            });
            return deferred.promise;
        };

    })
    .service('UtilsService',function(){

         this.logger = function() {

                console.log('logging service....');
    };
        this.returnFormattedDate = function(rawDateObj) {

        var d = new Date(rawDateObj),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
    };

    this.returnFormattedDateForService = function(rawDateObj) {

        var d = new Date(rawDateObj),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
    };


    this.returnDay = function(day){

        var dayString = '';

        switch (day){

            case 0:
            dayString = 'Sunday';
            break;
            case 1:
            dayString = 'Monday';
            break;
            case 2:
            dayString = 'Tuesday';
            break;
            case 3:
            dayString = 'Wednesday';
            break;
            case 4:
            dayString = 'Thursday';
            break;
            case 5:
            dayString = 'Friday';
            break;
            case 6:
            dayString = 'Saturday';
            break;
            default:
            dayString = '';
            break;
            
        }

        return dayString;
    };

    })
   