var app = angular.module('starter.signinpagecontroller', [])

//SignInCtrl
app.controller('SignInCtrl',function($scope,$rootScope,$state,$ionicLoading,LoginService,UserService,$cordovaNetwork,$localstorage, $rootScope) {
    $scope.remember='';
     console.log('Data ------', $scope.remember);
    
    if($localstorage.getObject('username')){
         $scope.user = {
         username : [$localstorage.getObject('username')],
         password : [$localstorage.getObject('password')]
    }; 
    }else {
        $scope.user = {};  
    }
  
    
    
    $scope.sessionObj='';
    $scope.signIn=function(userInfo){
      
           $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
     
         
               LoginService.loginAuth(userInfo)
            .then(function(response){
            console.log('success....',response);
            switch(response[0].errorCode) { 
                case "401" :
                    $ionicLoading.hide();
                    alert(response[0].errorMsg);
                 break;
                default :
                 $ionicLoading.hide();
                 UserService.setUser(response);
                 UserService.getUser();
                 $scope.sessionObj = UserService.getUser().userId;
                 $state.go('menu.home');
             }
            
            if($rootScope.rememberMe.checked) {
                 $localstorage.setObject('username',$scope.user.username);
                $localstorage.setObject('password',$scope.user.password);
            }  
             
             
            
           
            $scope.user = {};
         },function(error){
            $ionicLoading.hide();   
            alert("Invalid Credentials , please try again");
            $scope.user = {};
            console.log('error....');
             console.log(error);
            
        })
     
        
  };
 $scope.rememberMe=function(remember){
    
          if(remember) {
            $rootScope.rememberMe ={ text: "Remember Me", checked: true }; 
        }else {
             console.log('aff',remember);
          $rootScope.rememberMe ={ text: "Remember Me", checked: false };  
        }
   };
    
    
  })
    .controller('NavController', function($scope, $ionicSideMenuDelegate) {
      $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
      };
});
