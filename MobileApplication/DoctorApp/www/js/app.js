angular.module('starter', ['ionic','pascalprecht.translate','ngCordova', 'nvd3', 'starter.signinpagecontroller',
                           'starter.menucontroller',
                           'starter.homepagecontroller', 'starter.appointmentpagecontroller', 'starter.rosterPageController', 'starter.webservicesendpoints', 'starter.profilepagecontroller','services','ionic.utils'])

    .run(function ($ionicPlatform,$state,$ionicHistory,$localstorage,$rootScope) {
    
        $ionicPlatform.ready(function () {
           if($localstorage.getObject('username') !='') {
                   $rootScope.rememberMe ={ text: "Remember Me", checked: true };
            }else {
                 $rootScope.rememberMe ={ text: "Remember Me", checked: false };
             }
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                 StatusBar.styleDefault();
            }
            
          });
    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider,$translateProvider) {
    
        for(lang in translations){
        
           $translateProvider.translations(lang, translations[lang]);
        }
        $translateProvider.preferredLanguage('es');
        $httpProvider.defaults.withCredentials = true;
        $stateProvider
            .state('userlogin', {
                url: '/userlogin',
                templateUrl: 'templates/userlogin.html',
                controller: 'SignInCtrl'
            })
            .state('menu', {
                url: '/menu',
                templateUrl: 'templates/menu.html',
                abstract: true,
                controller: 'MenuController'
            })
            .state('menu.home', {
                url: '/home',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-home.html',
                        controller: 'HomePageController'
                    }
                }
            })
            .state('menu.profile', {
                url: '/profile',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-profile.html',
                         controller: 'ProfilePageController'
                     }
                }
            })
            .state('menu.appointment', {
                url: '/appointment',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-appointment.html',
                        controller: 'AppointmentPageController'
                    }
                }
            })
            .state('menu.roster', {
                url: '/roster',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-roster.html',
                        controller: 'RosterPageController'
                    }
                }
            })

        $urlRouterProvider.otherwise('/userlogin');
    });
