var app = angular.module('starter.rosterPageController', [])

//SignInCtrl
app.controller('RosterPageController',function($scope,$ionicModal,$ionicLoading,UtilsService,RosterService,$ionicPlatform,$ionicScrollDelegate) {
    

    $scope.rosters = [];
    $scope.indexedDays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    $scope.currentday = UtilsService.returnDay(new Date().getDay());
    $scope.stocksDetails = {
        
        stocksListArray : []
    };
    $scope.listofpharmacies = {
        branchDetailsList : []
    };

    $scope.$on('$ionicView.afterEnter', function(){
        highlightCurrentDay($scope.indexedDays);
        var dateObj = UtilsService.returnDay(new Date().getDay());
        RosterService.getRosterData(dateObj).then(function(response){

           $scope.rosters=response.doctorsClinicFinalList;
           console.log($scope.rosters);
        },function(error){
            console.log('error');
        });
       
    });

    function highlightCurrentDay(today){

        for(var i=0 ; i<today.length;i++){

            if(today[i] == $scope.currentday)
            {
                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#4CB050";
                elem[0].style.color = "#fff";

            }else{

                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#fff";
                elem[0].style.color = "#000";
            }

        }
    };

    function deselectAllDay(today){

        for(var i=0 ; i<today.length;i++){

                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#fff";
                elem[0].style.color = "#000";


        }
    };

    function highlightTabIndex(dayName){

 
        switch(dayName){
            case 'Sunday':
            var elem = angular.element(document.getElementById("Sunday"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            break;
            case 'Monday':
            var elem = angular.element(document.getElementById("Monday"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            break;
            case 'Tuesday':
            var elem = angular.element(document.getElementById("Tuesday"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            break;
            case 'Wednesday':
            var elem = angular.element(document.getElementById("Wednesday"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            break;
            case 'Thursday':
            var elem = angular.element(document.getElementById("Thursday"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            break;
            case 'Friday':
            var elem = angular.element(document.getElementById("Friday"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            break;
            case 'Saturday':
            var elem = angular.element(document.getElementById("Saturday"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            break;
            default:
            break;
        }


    };
   
    $scope.selectThisDay = function(selectedDay){

        deselectAllDay($scope.indexedDays);
        highlightTabIndex(selectedDay);
       RosterService.getRosterData(selectedDay).then(function(response){

           $scope.rosters=response.doctorsClinicFinalList;
           console.log($scope.rosters);
        },function(error){
            console.log('error');
        });
    };


    $scope.getSessionColor = function(sessionType){


        var sessionbgcolor = {'background-color':'blue'};
        switch(sessionType){
                case 0:
                sessionbgcolor = {'background-color':'#4CB050'};
                break;
                case 1:
                    sessionbgcolor = {'background-color':'#E40066'};
                break;
                case 2:
                    sessionbgcolor = {'background-color':'#004777'};
                break;
                case 3:
                    sessionbgcolor = 'session4-color';
                break;
                default:
                    sessionbgcolor = 'session1-color';
                break;

           
        }
         return sessionbgcolor;

    };

})