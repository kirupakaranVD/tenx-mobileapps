var app = angular.module('starter.signinpagecontroller', [])

//SignInCtrl
app.controller('SignInCtrl',function($scope,$rootScope,$state,$ionicLoading,LoginService,UserService,$cordovaNetwork) {

    console.log();
    $scope.user = {};
    
    $scope.signIn=function(userInfo){

// if(navigator.connection.type === "none"){
   
//    alert("No Internet Connectivity");
   
   
// }else{
             $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
        LoginService.loginAuth(userInfo)
            .then(function(response){
            console.log('success....',response);
            switch(response[0].errorCode)
            {
                    
                case "401" :
                    $ionicLoading.hide();
                    alert(response[0].errorMsg);
                    
                break;
                    
                default :
                 $ionicLoading.hide();
                 UserService.setUser(response);
                 UserService.getUser();
                $state.go('menu.home');

            }
            $scope.user = {};
            
        },function(error){
            $ionicLoading.hide();   
            alert("Invalid Credentials , please try again");
            $scope.user = {};
            console.log('error....');
             console.log(error);
            
        })

    // }
};





})
    .controller('NavController', function($scope, $ionicSideMenuDelegate) {
      $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
      };
});
