var app = angular.module('starter.menucontroller', []);

app.controller('MenuController', function ($scope, $state, $ionicSideMenuDelegate, $ionicLoading, LogoutService) {


    $scope.logout = function () {

        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0,
            duration: 5000
        });

        setTimeout(function () {
            $scope.$apply(function () {
                $ionicLoading.hide();
                LogoutService.logout().then(function (response) {
                    console.log('success logging out');
                    $ionicSideMenuDelegate.toggleLeft();
                    $state.go('userlogin');
                }, function (error) {
                    console.log('logout error....');
                });

            });
        }, 1000);


    };

})
