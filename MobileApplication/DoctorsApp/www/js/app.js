angular.module('starter', ['ionic','pascalprecht.translate','ngCordova', 'nvd3', 'starter.signinpagecontroller',
                           'starter.menucontroller',
                           'starter.homepagecontroller', 'starter.appointmentpagecontroller', 'starter.rosterPageController', 'starter.webservicesendpoints', 'services'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            
            
            
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider,$translateProvider) {
    
        for(lang in translations){
        
           $translateProvider.translations(lang, translations[lang]);
        }
        $translateProvider.preferredLanguage('en');
        $httpProvider.defaults.withCredentials = true;
        $stateProvider
            .state('userlogin', {
                url: '/userlogin',
                templateUrl: 'templates/userlogin.html',
                controller: 'SignInCtrl'
            })
            .state('menu', {
                url: '/menu',
                templateUrl: 'templates/menu.html',
                abstract: true,
                controller: 'MenuController'
            })
            .state('menu.home', {
                url: '/home',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-home.html',
                        controller: 'HomePageController'
                    }
                }
            })
            .state('menu.appointment', {
                url: '/appointment',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-appointment.html',
                        controller: 'AppointmentPageController'
                    }
                }
            })
            .state('menu.roster', {
                url: '/roster',
                views: {
                    'menu': {
                        templateUrl: 'templates/menu-roster.html',
                        controller: 'RosterPageController'
                    }
                }
            })

        $urlRouterProvider.otherwise('/userlogin');
    });
