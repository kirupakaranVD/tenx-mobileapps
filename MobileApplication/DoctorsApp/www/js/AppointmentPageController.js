var app = angular.module('starter.appointmentpagecontroller', []);

app.controller('AppointmentPageController', function ($scope, $ionicModal, $ionicPopup, $ionicLoading, $cordovaDatePicker,UserService, AppointmentService,ClinicService, $translate,UtilsService) {

    
    $scope.sessionArray = ['AllSession', 'Session1', 'Session2', 'Session3','Session4'];
    $scope.ownclinic = {};
    $scope.childCount;
    $scope.formattedDate;
    $scope.listofclinics = {
        clinicDetailList: []
    };
    $scope.appointmentFilter={
        'query':''
    };
    $scope.clinicObj = {
        'clinicName':'Select Clinic'
    };
    $scope.selectedClinicObj = {};
    $scope.appointments = [];
    $scope.sessionObj = {
        'value':'All Session'
    };
     $scope.choice = {
        val: 'All'
    };
    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.showSessionPopup = showSessionPopup;

    $scope.showClinicModal = function () {

        $scope.modal.show();
    };


    $scope.getSessionCode = function(sessionObj){
         var retVal = "";
        switch(sessionObj){
            case 'Session1':
            retVal='S1';
            break;
            case 'Session2':
            retVal='S2';
            break;
            case 'Session3':
            retVal='S3';
            break;
            case 'Session4':
            retVal='S4';
            break;
            default :
            retVal = "";
            break;
        }

        return retVal;
    };

    $scope.geticonType = function(genderType){
    
    if(genderType == "Male"){
        return 'ion-male';
    }else{
        return 'ion-male';
    }

    };


    $scope.$on('$ionicView.afterEnter', function () {

       highlightCurrentDay($scope.sessionArray);
       var formattedDate = UtilsService.returnFormattedDate(new Date());
        $scope.formattedDate = formattedDate;
       console.log('formatted date is ............',formattedDate);
        var obj_clinic = UserService.getUser();
        console.log(obj_clinic)
        $scope.ownclinic.name = obj_clinic.ownclinic;
         $scope.clinicObj.clinicName = obj_clinic.ownclinic;
         console.log('clinic object....',$scope.clinicObj);
        AppointmentService.getOwnClinicAppointment().then(function (response) {
            console.log('appointment details.....', response.bookAppointmentOpen);
            $scope.appointments = response.bookAppointmentOpen;
            ClinicService.getClinicList().then(function(response){
                console.log('clinics list.....',response.doctorsClinicList);
                $scope.listofclinics.clinicDetailList=response.doctorsClinicList;
            },function(error){
                console.log('clinic list error....',error);
            });
            $ionicLoading.hide();
        }, function (error) {
            console.log('error');
            $ionicLoading.hide();
        });

    });

    $scope.setOwnClinic = function(){
                console.log('$scope.ownclinic.name.....' , $scope.ownclinic.name);
             $scope.clinicObj.clinicName=$scope.ownclinic.name;
              AppointmentService.getOwnClinicAppointment().then(function (response) {
            console.log('appointment details.....', response.bookAppointmentOpen);
            
            $scope.appointments = response.bookAppointmentOpen;
            $scope.appointmentFilter.query = "";
            $scope.sessionObj.value = 'All Session';

            $scope.modal.hide();
        }, function (error) {
            console.log('error');
            $scope.modal.hide();
            $ionicLoading.hide();
        });


    };
    
    $scope.setClinic = function(clinicData){
            console.log(clinicData);
              
              AppointmentService.getClinicAppointment(clinicData).then(function (response) {
            console.log('clinics appointment details.....', response.bookAppointmentOpen);
           
            $scope.appointments = response.bookAppointmentOpen;
             $scope.appointmentFilter.query = "";
            if(clinicData.organizationName && clinicData.organizationId)
            {
                $scope.selectedClinicObj = clinicData;
                console.log('selected clinic object...',$scope.selectedClinicObj);
            }
            if(clinicData.organizationName){
                $scope.clinicObj.clinicName=clinicData.organizationName;
            }else{
                $scope.clinicObj.clinicName="Pharmacy";
            }
            
            $scope.modal.hide();
        }, function (error) {
            console.log('error');
            $scope.modal.hide();
            $ionicLoading.hide();
        });


    };

    function showSessionPopup() {
        console.log($scope.branchObj);
        
            $ionicPopup.show({
            title: '',
            templateUrl: 'templates/report-session-template.html',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel',
                    type: 'button-stable',
                    onTap: function (e) {
                       
                        console.log($scope.sessionObj.value);
                    }
                        },
                {
                    text: 'Ok',
                    type: 'button-balanced',
                    onTap: function (e) {
                        console.log($scope.choice);
                         $scope.childCount = angular.element(document.querySelector("#appointment-container"))[0].childElementCount;
                         console.log('previoussibling',angular.element(document.querySelector("#appointment-container"))[0].childElementCount)
                        console.log('total childcount....',$scope.childCount);

                        return $scope.choice;
  
                    }

                    }
                    ]

        });


    };

    $scope.showCalender = function(){

        var options = {
    date: new Date(),
    mode: 'date', // or 'time'
    minDate: new Date() - 10000,
    allowOldDates: true,
    allowFutureDates: false,
    doneButtonLabel: 'DONE',
    doneButtonColor: '#F2F3F4',
    cancelButtonLabel: 'CANCEL',
    cancelButtonColor: '#000000'
  };

 
  document.addEventListener("deviceready", function () {

    $cordovaDatePicker.show(options).then(function(date){
        $scope.appointmentFilter.query = "";
        var appointmentDate = formatDate(date);
        $scope.formattedDate = UtilsService.returnFormattedDate(date);
        if($scope.selectedClinicObj.organizationId){
            console.log('exec getChildClinicAppointmentFromDate of orgId....',$scope.selectedClinicObj.organizationId);
            getChildClinicAppointmentFromDate($scope.selectedClinicObj.organizationId,appointmentDate);
        }else{
            console.log('exec getMasterClinicAppointmentFromDate of orgId....',$scope.selectedClinicObj.organizationId);
            getMasterClinicAppointmentFromDate(appointmentDate);
        }
        
    });

  }, false);


    };


    function formatDate(dateObj) {
    var d = new Date(dateObj),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};


    function getChildClinicAppointmentFromDate(clinicData,dateOfAppointment){

            console.log('checking clinicswitch id....',clinicData);
        AppointmentService.getChildClinicAppointmentFromDate(clinicData,dateOfAppointment).then(function(response){
            
            $scope.appointments = response.bookAppointmentOpen;
            
            console.log('getChildClinicAppointmentFromDate......',$scope.appointments);
        },function(error){
            alert('can\'t fetch data, please try again');
        })
    };
    function getMasterClinicAppointmentFromDate(dateOfAppointment){

        AppointmentService.getMasterClinicAppointmentFromDate(dateOfAppointment).then(function(response){
            $scope.appointments = response.bookAppointmentOpen;
            
            console.log('getClinicAppointmentFromDate......',$scope.appointments);
        },function(error){
            alert('can\'t fetch data, please try again');
        })
    };




    // $scope.checkChildCount = function (){
    //         console.log($scope.choice);
    //            $scope.sessionObj.value = $scope.choice.val;
    //                    if($scope.choice.val == "All Session"){
                            
    //                         $scope.appointmentFilter.query = "";

    //                    }else{
    //                        $scope.appointmentFilter.query = $scope.choice.val;
    //                    }
        
    // };


    $scope.selectThisSession = function(selectedSession){

        deselectAllDay($scope.sessionArray);
        highlightTabIndex(selectedSession);
    //     $scope.childCount = angular.element(document.querySelector("#appointment-container"))[0].childElementCount;
    //    console.log($scope.childCount)
    };

    function highlightCurrentDay(today){

        for(var i=0 ; i<today.length;i++){

            if(today[i] == 'AllSession')
            {
                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#4CB050";
                elem[0].style.color = "#fff";
                $scope.appointmentFilter.query = "";

            }else{

                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#fff";
                elem[0].style.color = "#000";
            }

        }
    };

    function deselectAllDay(today){

        for(var i=0 ; i<today.length;i++){

                var elem = angular.element(document.getElementById(today[i]));
                elem[0].style.backgroundColor = "#fff";
                elem[0].style.color = "#000";


        }
    };

    function highlightTabIndex(dayName){

 
        switch(dayName){
            case 'AllSession':
            var elem = angular.element(document.getElementById("AllSession"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "";
            break;
            case 'Session1':
            var elem = angular.element(document.getElementById("Session1"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session1";
            break;
            case 'Session2':
            var elem = angular.element(document.getElementById("Session2"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session2";
            break;
            case 'Session3':
            var elem = angular.element(document.getElementById("Session3"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session3";
            break;
            case 'Session4':
            var elem = angular.element(document.getElementById("Session4"));
            elem[0].style.backgroundColor = "#4CB050";
            elem[0].style.color = "#fff";
            $scope.appointmentFilter.query = "Session4";
            break;
            default:
            break;
        }


    };


})